from collections import Container
from line_profiler import LineProfiler
from functools import wraps
from datetime import datetime
import os


def flatten(obj):
    for item in obj:
        if isinstance(item, Container):
            yield from flatten(item)
        else:
            yield item


def lp(fn):
    @wraps(fn)
    def profile(*args, **kwargs):
        lp = LineProfiler()
        lp_wrapper = lp(fn)
        result = lp_wrapper(*args, **kwargs)
        lp.print_stats()
        return result
    return profile


def whole_months_between(date1, date2) -> int:
    """
    computes number of complete months between two dates
    :rtype: int
    """
    args = [date1, date2]
    if all(args):
        d1 = min(date1, date2)
        d2 = max(date1, date2)
        n_months = abs((d2.year - d1.year) * 12 +
                       d2.month - d1.month -
                       (0 if d2.day >= d1.day else 1))

        return n_months


def beep():
    duration = 1  # seconds
    freq = 200  # Hz
    os.system('play -nq -t alsa synth {} sine {}'.format(duration, freq))

