#!/bin/bash
# columns in file: 5
# columns 1-3: irrelevant
# columns 4-5: contain two dates in dd.mm.yyyy format
# field separator: ","
# line 1: header
# 
# objective:
# change dates format to yyyy-mm-dd
# add +1 column to the right with difference between two dates in days
# 
# example. file_input
# a,b,c,d1,d2
# 1,2,3,01.05.2012,04.11.1956
# 2,3,4,25.09.2018,29.02.2020
# 
# example. file_output
# a,b,c,d1,d2,diff
# 1,2,3,2012-05-01,1956-11-04,20266
# 2,3,4,2018-09-25,2020-02-29,-522

echo $(head -n 1 file_input),diff > file_output; paste -d , <(tail +2 file_input | cut -d , -f1-3) <(tail +2 file_input | cut -d , -f4- | tr ',' ' ' | sed -E "s/([0-9]+).([0-9]+).([0-9]+)/\3-\2-\1/g" | xargs -n2 sh -c 'diff=$(echo "(`date -d $0 +%s` - `date -d $1 +%s`) / (24*3600)" | bc); echo $0,$1,$diff') >> file_output
