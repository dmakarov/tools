"""
merging horizontally csv files by first column with bash commands
"""
import subprocess
import os
from typing import Dict


def engineer(config: Dict[str, str]) -> None:
    """
    creates N csv files where (N+1) - number of columns in the file_targets
    joins by first column in files
    drops lines where file_targets index or target values are empty
    """
    task_id = config['task_id']
    file_targets = config['file_targets']
    file_features = config['file_features']
    sep_targets = config['sep_targets']
    sep_features = config['sep_features']
    result_dir = config['result_dir']
    try:
        os.mkdir(result_dir)
    except FileExistsError:
        pass

    with open(file_targets) as f:
        header = f.readline().strip().split(sep_targets)
    if len(header) < 2:
        raise ValueError('wrong header')

    cmd_features = f'head -n 1 {file_features}; ' \
                   f'tail -n +2 {file_features} | ' \
                   f'sort -s -k 1 -t {sep_features} | ' \
                   f'awk -F {sep_features} \'$1!=""\''
    for i, target in enumerate(header[1:], 2):
        file_res = result_dir + task_id + f'_{target}.csv'
        cmd_targets = f'cut -d {sep_targets} -f 1,{i} {file_targets} | ' \
                      f'head -n 1; ' \
                      f'cut -d {sep_targets} -f 1,{i} {file_targets} | ' \
                      f'tail -n +2 | ' \
                      f'sort -s -k 1 -t {sep_targets} | ' \
                      f'awk -F {sep_targets} \'$1!="" && $2!=""\''
        cmd_join = f'join -1 1 -2 1 -t {sep_features} --nocheck-order --header <({cmd_targets}) <({cmd_features})' \
                   f' > {file_res}'
        subprocess.Popen(cmd_join, shell=True, stdout=subprocess.PIPE, executable="/bin/bash").stdout.read()


# params = {
#     'task_id': 'mytest',
#     'file_targets': '/home/tern/Work/tools/test/calc_status_527_g9.csv',
#     'file_features': '/home/tern/Work/tools/test/calc_vars_527_g9.csv',
#     'result_dir': '/home/tern/Work/tools/test/res/',
#     'sep_targets': ',',
#     'sep_features': ','
# }
#
#
# from datetime import datetime
# start = datetime.now()
# engineer(params)
# print(datetime.now() - start)
