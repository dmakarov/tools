from datetime import datetime


file_status = '../test/calc_status_527_g9.csv'
file_features = '../test/calc_vars_527_g9.csv'
sep = ','


def gen_open(source):
    return open(source)


def get_lines(opened):
    for line in opened:
        yield line


def get_requests(lines):
    for line in lines:
        yield line.split(sep, maxsplit=1)[0]


def get_status(lines, n):
    for line in lines:
        splitted = line.split(sep)
        yield splitted[0], splitted[n]


features_file = open(file_features)
features_lines = (x for x in get_lines(features_file))
features_requests = (x for x in get_requests(features_file))
features_set = set(features_requests)

start = datetime.now()
status_file = open(file_status)
status_lines = (x for x in get_lines(status_file))
status = (x for x in get_status(status_lines, 2))


for i in status:
    print(i)

# status_requests = (x for x in get_requests(status_lines))

# features_file = open(file_features)
# features_lines = (x for x in get_lines(features_file))
# features_requests = (x for x in get_requests(features_file))

# for i in status_requests:
#     print(i[0])
# set1 = set(status_requests)
# set2 = set(features_requests)
# print(set1)
# print(len(set1))
# print(set2)
# print(len(set2))
# intersection = set1.intersection(set2)
# print(intersection)
# print(len(intersection))


# for i in get_line(status_requests):
#     print(i)
# print(len([x for x in status_requests]))
# print(datetime.now() - start)
#
# status_file.close()
features_requests.close()
