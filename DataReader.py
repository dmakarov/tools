from datetime import datetime


class DataReader:
    """credit history reader contained in csv table"""
    def __init__(self, file, sep='‰', encoding='cp1251'):
        self.file = file
        self.sep = sep
        self.encoding = encoding
        self.fields = (
            'accountRating',
            'accountRatingDate',
            'acctType',
            'amtOutstanding',
            'amtPastDue',
            'c_time',
            'ch_id',
            'closedDt',
            'creditLimit',
            'curBalanceAmt',
            'fileSinceDt',
            'interestPaymentDueDate',
            'interestPaymentFrequencyCode',
            'lastPaymtDt',
            'lastUpdatedDt',
            'monthsReviewed',
            'numDays30',
            'numDays60',
            'numDays90',
            'openedDt',
            'paymentDueDate',
            'paymtPat',
            'paymtPatStartDt',
            'report_c_time',
            'reportingDt',
            'request_id',
            'termsAmt',
            'termsFrequency'
        )
        self.fields_dates = (
            'accountRatingDate',
            'c_time',
            'closedDt',
            'fileSinceDt',
            'interestPaymentDueDate',
            'lastPaymtDt',
            'lastUpdatedDt',
            'openedDt',
            'paymentDueDate',
            'paymtPatStartDt',
            'report_c_time',
            'reportingDt'
        )

    def read(self, file=None, encoding=None, sep=None):
        """reads csv table file and stores it in dictionary of {request_id: accounts2} format"""
        if not file:
            file = self.file
        if not encoding:
            encoding = self.encoding
        if not sep:
            sep = self.sep

        with open(file, encoding=encoding) as f:
            header = f.readline().strip().split(sep)
            result = {}
            for i, line in enumerate(f):
                line = line.strip().split(sep)
                if len(header) != len(line):
                    print('there is something wrong with header and line lengths: line ', i)
                    continue
                local = {k: v for k, v in zip(header, line)}
                request_id = local['request_id']
                if request_id not in result:
                    result[request_id] = []
                result[request_id].append({k: v for k, v in zip(header, line)})

            self.data = result

    @staticmethod
    def _remove_fields(n, fields):
        return {k: v for k, v in n.items() if k in fields}

    @staticmethod
    def _replace_commas(n):
        return {k: v.replace(',', '.') for k, v in n.items()}

    @staticmethod
    def _strip_quotes(n):
        return {k: v.strip('"') for k, v in n.items()}

    @staticmethod
    def _to_empty_str(n):
        return {k: '' if v == 'null' else v for k, v in n.items()}

    @staticmethod
    def _to_iso_date(date):
        if date == '':
            return date
        return datetime.strptime(date.split()[0], '%d.%m.%Y').strftime('%Y-%m-%d')

    def _to_dates(self, n):
        return {k: self._to_iso_date(v) if k in self.fields_dates else v for k, v in n.items()}

    def process(self):
        data = self.data
        for request_id, accounts2 in data.items():
            for i, node in enumerate(accounts2):
                node = self._remove_fields(node, self.fields)
                node = self._replace_commas(node)
                node = self._strip_quotes(node)
                node = self._to_empty_str(node)
                node = self._to_dates(node)
                accounts2[i] = node
            data[request_id] = accounts2
        self.data = data
